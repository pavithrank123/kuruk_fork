var bodyParser = require('body-parser');
var User = require('../models/user');
module.exports.regusers = function(req,res) {
    try {
        var name = req.body.name;
        var email = req.body.email;
        var username = req.body.username;
        var password = req.body.password;
        var password2 = req.body.password2;
        // Validation
        req.checkBody('name', 'Name is required').notEmpty();
        req.checkBody('email', 'Email is required').notEmpty();
        req.checkBody('email', 'Email is not valid').isEmail();
        req.checkBody('username', 'Username is required').notEmpty();
        req.checkBody('password', 'Password is required').notEmpty();
        req.checkBody('password2', 'Passwords do not match').equals(req.body.password);

        var errors = req.validationErrors();

        if (errors) {
            res.render('register', {
                errors: errors
            });
        } else {
            var newUser = new User({
                name: name,
                email: email,
                username: username,
                password: password
            });

            User.createUser(newUser, function (err, user) {
                if (err) {
                    throw err;
                }
                console.log(user);
            });

            req.flash('success_msg', 'You are registered and can now login');

            res.redirect('/users/login');
        }
    }
    catch(err)
    {
        console.log(err);
        console.log('ERROR');
    }
};
module.exports.quizcalc=function (req,res) {
    try {
        var answer1 = req.body.ans1;
        var answer2 = req.body.ans2;
        var answer3 = req.body.ans3;
        var answer4 = req.body.ans4;
        var answer5 = req.body.ans5;

        //validation checks
        req.checkBody('ans1', 'Guess The First LOGO').notEmpty();
        req.checkBody('ans2', 'Guess The Second LOGO').notEmpty();
        req.checkBody('ans3', 'Guess The Third LOGO').notEmpty();
        req.checkBody('ans4', 'Guess The Fourth LOGO').notEmpty();
        req.checkBody('ans5', 'Guess The Fifth LOGO').notEmpty();

        var errors = req.validationErrors();
        var answer=0;
        if (errors) {
            res.render('quiz', {
                errors: errors
            });
        }
        else
        {
            //Calculating the answers
            if(req.body.ans1=='msn')
            {
                answer++;
            }
            if(req.body.ans2=='avira')
            {
                answer++
            }
            if(req.body.ans3=='skype')
            {
                answer++;
            }
            if(req.body.ans4=='snapchat')
            {
                answer++;
            }
            if(req.body.ans5=='messenger')
            {
                answer++;
            }
            console.log('Quiz Successful without any ERRORS');
          /*  window.onload=function(){
            document.getElementById("CorrectAnswer").innerHTML=answer;
        };*/

            var answer = module.exports;
            res.redirect('/users/exit');
        }
    }
    catch(err)
    {
        console.log(err);
    }
};